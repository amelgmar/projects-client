interface Project {
    id?: number;
    project_name?: string,
    project_number?: string,
    acquisition_date?: string,
    number_3l_code?: string,
    project_deal_type_id?: string,
    project_group_id?: string,
    project_status_id?: string,
    wtg_numbers?: string,
    total_kw?: number,
    months_acquired?: number
}
