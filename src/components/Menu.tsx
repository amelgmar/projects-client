import React, {FC} from "react";
import styled from "styled-components";
import HomeIcon from "@mui/icons-material/Home";
import {Link, useNavigate} from "react-router-dom";
const Container = styled.div`
  flex: 1;
  background-color: ${({ theme }) => theme.bgLighter};
  height: 100vh;
  color: ${({ theme }) => theme.text};
  font-size: 14px;
  position: sticky;
  top: 0;
`;
const Wrapper = styled.div`
  padding: 18px 26px;
`;
const Logo = styled.div`
  display: flex;
  align-items: center;
  gap: 5px;
  font-weight: bold;
  margin-bottom: 25px;
`;

const Item = styled.div`
  display: flex;
  align-items: center;
  gap: 20px;
  cursor: pointer;
  padding: 7.5px 0px;

  &:hover {
    background-color: ${({ theme }) => theme.soft};
  }
`;

const Hr = styled.hr`
  margin: 15px 0px;
  border: 0.5px solid ${({ theme }) => theme.soft};
`;

export interface MenuProps {
  darkMode?: boolean;
  setDarkMode?: (darkMode: boolean) => void;
}

const Menu: FC<MenuProps> =({ darkMode, setDarkMode }) => {
  const navigate = useNavigate()

  return (
    <Container>
      <Wrapper>
        <Link to="/" style={{ textDecoration: "none", color: "inherit" }}>
          <Logo>
            Projects List
          </Logo>
        </Link>
        <Link
            to="/"
            style={{ textDecoration: "none", color: "inherit" }}
        >
        <Item>
          <HomeIcon />
          Projects
        </Item>
        </Link>
        <Hr />
      </Wrapper>
    </Container>
  );
};

export default Menu;
