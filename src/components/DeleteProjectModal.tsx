import React, {FC} from "react";
import styled from "styled-components";

export interface DeleteAuthorModalProps {
    project: Project | undefined;
    onClose?: () => void;
    onDelete?: (project: Project | undefined) => void;
}
const Container = styled.div`
  width: 100%;
  height: 100%;
  position: fixed;
  top: 0;
  left: 0;
  background-color: #000000a7;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 9;
`;

const Wrapper = styled.div`
  width: 600px;
  height: 600px;
  background-color: ${({ theme }) => theme.bgLighter};
  color: ${({ theme }) => theme.text};
  padding: 20px;
  display: flex;
  flex-direction: column;
  gap: 20px;
  position: relative;
`;
const Title = styled.h2`
  text-align: center;
`;
const DeleteAuthorModal: FC<DeleteAuthorModalProps> = ({ project, onClose, onDelete }) => {
    const handleDelete = () => {
        if (onDelete) {
            onDelete(project);
        }
    };

    return (
        <Container>
            <Wrapper>
                <Title>Delete Project</Title>
            <div>Are you sure you want to delete project "{project?.project_name}"?</div>
            <div>
                <button onClick={onClose}>Cancel</button>
                <button onClick={handleDelete}>Delete</button>
            </div>
        </Wrapper>
            </Container>
    );
}

export default DeleteAuthorModal;