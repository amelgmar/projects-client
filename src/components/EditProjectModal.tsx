import React, {FC, useState} from "react";
import styled from "styled-components";

const Select = styled.select`
  border: 1px solid ${({ theme }) => theme.soft};
  color: ${({ theme }) => theme.text};
  border-radius: 3px;
  padding: 10px;
  background-color: transparent;
  z-index: 999;
`;
const Container = styled.div`
  width: 100%;
  height: 100%;
  position: fixed;
  top: 0;
  left: 0;
  background-color: #000000a7;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 9;
`;

const Wrapper = styled.div`
  width: 600px;
  height: 600px;
  background-color: ${({ theme }) => theme.bgLighter};
  color: ${({ theme }) => theme.text};
  padding: 20px;
  display: flex;
  flex-direction: column;
  gap: 20px;
  position: relative;
`;
const Title = styled.h2`
  text-align: center;
`;
export interface EditProjectModalProps {
    project: Project | undefined;
    onClose?: () => void;
    onSave?: (project: Project) => void;
}
export const EditProjectModal: FC<EditProjectModalProps> = ({ project, onClose, onSave }) => {

    const [name, setName] = useState(project?.project_name);
    const [projectNumber, setProjectNumber] = useState(project?.project_number);
    const [acquisitionDate, setAcquisitionDate] = useState(project?.acquisition_date);
    const [numberCode, setNumberCode] = useState(project?.number_3l_code);
    const [dealType, setDealType] = useState(project?.project_deal_type_id);
    const [groupId, setGroupId] = useState(project?.project_group_id);
    const [statusId, setStatusId] = useState(project?.project_status_id);

    const handleSave = () => {
        if (onSave) {
            onSave({
                ...project, project_name: name, project_number:
                projectNumber, acquisition_date: acquisitionDate,
                number_3l_code: numberCode,
                project_deal_type_id: dealType,
                project_group_id: groupId,
                project_status_id: statusId

            });
        }
    };

    // @ts-ignore
    return (
        <Container>
            <Wrapper>
                <Title>Edit Project</Title>
            <div>
                <label htmlFor="name">Project Name:</label>
                <input type="text" id="name" value={name} onChange={(e) => setName(e.target.value)} />
            </div>
            <div>
                <label htmlFor="projectNumber">ProjectNumber:</label>
                <input type="text" id="projectNumber" value={projectNumber} onChange={(e) => setProjectNumber(e.target.value)} />
            </div>
            <div>
                <label htmlFor="acquisitionDate">Acquisition Date:</label>
                <input type="date" id="projectNumber" value={acquisitionDate} onChange={(e) => setAcquisitionDate(e.target.value)} />
            </div>
            <div>
                <label htmlFor="number_code">Project Number 3L code:</label>
                <input type="text" id="number_code" value={numberCode} onChange={(e) => setNumberCode(e.target.value)} />
            </div>
            <div>
                <label htmlFor="deal_type">Project Deal Type:</label>
                <Select value={dealType} name="deal_type" id="deal_type" onChange={(e) => setDealType(e.target.value)}>
                    <option value="Share">Share</option>
                    <option value="Asset">Asset</option>
                </Select>
            </div>
            <div>
                <label htmlFor="group_id">Project Group Id:</label>
                <Select value={groupId} name="group_id" id="group_id" onChange={(e) => setGroupId(e.target.value)}>
                    <option value="RW 1">RW 1</option>
                    <option value="QE 4">QE 4</option>
                </Select>
            </div>
            <div>
                <label htmlFor="status_id">Project Status Id:</label>
                <Select value={statusId} name="status_id" id="status_id" onChange={(e) => setStatusId(e.target.value)}>
                    <option value="1 Operating">1 Operating</option>
                    <option value="2 DD">2 DD</option>
                </Select>
            </div>
            <div>
                <button onClick={onClose}>Cancel</button>
                <button onClick={handleSave}>Save</button>
        </div>
            </Wrapper>
        </Container>)
};
