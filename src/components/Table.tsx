import React, {useState, useEffect, useMemo, useCallback} from "react";
import styled from "styled-components";
import { useTheme } from '@table-library/react-table-library/theme';
import {EditProjectModal} from "./EditProjectModal";
import DeleteProjectModal from "./DeleteProjectModal";
import { usePagination } from "@table-library/react-table-library/pagination";
import {CompactTable} from "@table-library/react-table-library/compact";
import {useDeleteProject, useFetchProjects, useUpdateProject} from "../services/ProjectAPI";
import {ErrorDisplayed} from "./ErrorDisplayed";
import {Loader} from "./Loader";
import {debounce} from "@mui/material";
import {useSort} from "@table-library/react-table-library/sort";
import UnfoldMoreOutlinedIcon from '@mui/icons-material/UnfoldMoreOutlined';
import KeyboardArrowUpOutlinedIcon from '@mui/icons-material/KeyboardArrowUpOutlined';
import KeyboardArrowDownOutlinedIcon from '@mui/icons-material/KeyboardArrowDownOutlined';
import SportsBasketballOutlinedIcon from "@mui/icons-material/SportsBasketballOutlined";
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';

const Container = styled.div`
  display: flex;
  gap: 24px;
`;

const Content = styled.div`
  flex: 5;
`;
const Item = styled.div`
  display: flex;
  align-items: center;
  gap: 20px;
  cursor: pointer;
  padding: 7.5px 0px;

  &:hover {
    background-color: ${({ theme }) => theme.soft};
  }
`
export const Table = ({}) => {
    const THEME = {
        Table:`
        width: 100%
        `,
        HeaderRow: `
    font-size: 14px;

    background-color: #eaf5fd;
  `,
        Row: `
    font-size: 14px;

    &:nth-child(odd) {
      background-color: #d2e9fb;
    }

    &:nth-child(even) {
      background-color: #eaf5fd;
    }
  `,
    };
    const [showEditModal, setShowEditModal] = useState(false);
    const [showDeleteModal, setShowDeleteModal] = useState(false);
    const [selectedProject, setSelectedProject] = useState<Project>();
    const [editedProject, setEditedProject] = useState<Project>();
    const [deletedProject, setDeletedProject] = useState<Project>();

    // states for table
    const [allData, setAllData] = useState<Project[]>([])
    const [pageCount, setPageCount] = useState(null)
    const [pageNumber, setPageNumber] = useState(1)
    const [search, setSearch] = React.useState('');
    const [sortField, setSortField] = React.useState('');
    const [sortOrder, setSortOrder] = React.useState('');

    const { loading, error, results } = useFetchProjects(pageNumber, 2, search, sortField, sortOrder)

    const theme = useTheme(THEME);

    useUpdateProject(selectedProject?.id, editedProject, (newData: Project) => {
        // Automatically update the data after a successful Update request
        setAllData(prev => {
            if(prev) {
                const foundIndex = prev.findIndex(x => x.id == newData.id);
                if(foundIndex !== -1) {
                    prev[foundIndex] = newData as Project;
                }
            }
            return prev
        });
    })

    useDeleteProject(deletedProject?.id, (newData: Project) => {
        // Automatically update the data after a successful Update request
        setAllData(prev => {
            return prev.filter(f => f.id !== newData.id)
        });
    })

    useEffect(() =>{
        if (results) {
            setAllData(results.rows)
            setPageCount(results.pageCount)
        }
    }, [results])

    const dataNodes = { nodes: allData };

    const onPaginationChange = (index: number) => {
        setPageNumber(index + 1)

    }


    // @ts-ignore
    const pagination = usePagination(dataNodes, {
        state: {
            page: 0,
            size: 2,
        },
    });

    function onSortChange(action: any, state: any) {
        console.log(action, state);
    }


    // @ts-ignore
    const sort = useSort(
        dataNodes,
        {
            onChange: onSortChange,
        },
        {
            sortIcon: {
                margin: '0px',
                iconDefault: <UnfoldMoreOutlinedIcon />,
                iconUp: <KeyboardArrowUpOutlinedIcon />,
                iconDown: (
                    <KeyboardArrowDownOutlinedIcon />
                ),
            },
            sortFns: {

            },
        },

    );

    const COLUMNS = [
        { label: 'Project Name', renderCell: (item: { project_name: any; }) => item.project_name ,
            sort: { sortKey: "PROJECT_NAME" },  resize: true},
        {
            label: 'Project Number',
            renderCell: (item: { project_number: any; }) => item.project_number,
            sort: { sortKey: "PROJECT_NUMBER" },  resize: true
        },
        { label: 'Acquisition Date',  renderCell: (item: { acquisition_date: string}) =>
                item.acquisition_date,
            sort: { sortKey: "ACQUISITION_DATE" },  resize: true
        },
        {
            label: 'Project 3l Code',
            renderCell: (item: { number_3l_code: any; }) => item.number_3l_code,
            sort: { sortKey: "PROJECT_3L_CODE" },  resize: true
        },
        {
            label: 'Project Deal Type',
            renderCell: (item: { project_deal_type_id: any; }) => item.project_deal_type_id,
            sort: { sortKey: "PROJECT_DEAL" },  resize: true
        },
        {
            label: 'Project Group',
            renderCell: (item: { project_group_id: any; }) => item.project_group_id,
            sort: { sortKey: "PROJECT_GROUP" },  resize: true
        },
        {
            label: 'Project Status',
            renderCell: (item: { project_status_id: any; }) => item.project_status_id,
            sort: { sortKey: "PROJECT_STATUS" },  resize: true
        },
        {
            label: 'Wtg numbers',
            renderCell: (item: { wtg_numbers: any; }) => item.wtg_numbers,  resize: true
        },
        {
            label: 'Total KW',
            renderCell: (item: { total_kw: any; }) => item.total_kw,
            sort: { sortKey: "TOTAL_KW" },  resize: true
        },
        {
            label: 'Months acquired',
            renderCell: (item: { months_acquired: any; }) => item.months_acquired,
            sort: { sortKey: "MONTHS_ACQUIRED" },  resize: true
        },
        { label: 'Actions', renderCell: (item: { project_name: React.SetStateAction<null>; }) => <div>
                <Item onClick={() => handleEdit(item)}><EditIcon /></Item>
                <Item onClick={() => handleDelete(item)}><DeleteIcon /></Item>
            </div> ,  resize: true},
    ];

    const handleEdit = (row: any) => {
        setSelectedProject(row);
        setShowEditModal(true);
    };

    const handleDelete = (row: any) => {
        setSelectedProject(row);
        setShowDeleteModal(true);
    };

    const handleCloseEditModal = () => {
        setSelectedProject(undefined);
        setShowEditModal(false);
    };

    const handleCloseDeleteModal = () => {
        setSelectedProject(undefined);
        setShowDeleteModal(false);
    };

    const handleSaveEditModal = async (project: any) => {
        setEditedProject({project_name: project.project_name,
            project_number: project.project_number,
            acquisition_date: project.acquisition_date,
            number_3l_code: project.number_3l_code,
            project_deal_type_id: project.project_deal_type_id,
            project_group_id: project.project_group_id,
            project_status_id: project.project_status_id,
        })
        setShowEditModal(false);
    };

    const handleDeleteProject = async (project: any) => {
        setDeletedProject(project)
        setSelectedProject(undefined);
        setShowDeleteModal(false);
    };

    const handleSearch = (event: { target: { value: React.SetStateAction<string>; }; }) => {
        setPageNumber(1)
        setSearch(event.target.value);
    };

    const debouncedChangeHandler = useCallback(
        debounce(handleSearch, 500)
        , []);


    // @ts-ignore
    return (
        <Container>
            <Content>
                {error ?
                    <ErrorDisplayed message={error.message}/> :

                    loading ?
                        <Loader/> :
                        <>
                            <label htmlFor="search">
                                Search by Task:
                                <input id="search" type="text" onChange={debouncedChangeHandler}/>
                            </label>
                            <CompactTable columns={COLUMNS} data={dataNodes}
                                          pagination={pagination} theme={theme} sort={sort}/>
                            <div
                                style={{ display: 'flex', justifyContent: 'space-between' }}
                            >
    <span>
      Total Pages: {pageCount}
    </span>

                                <span>
          Page:{' '}
                                    {[...Array(pageCount)].map((_: any, index: number) => (
                                        <button
                                            key={index}
                                            type="button"
                                            style={{
                                                fontWeight:
                                                    pagination.state.page === index
                                                        ? 'bold'
                                                        : 'normal',
                                            }}
                                            onClick={() => onPaginationChange(index)}
                                        >
                                            {index + 1}
                                        </button>
                                    ))}
        </span>
                            </div>
                            {showEditModal && (
                                <EditProjectModal
                                    project={selectedProject}
                                    onClose={handleCloseEditModal}
                                    onSave={handleSaveEditModal}
                                />
                            )}
                            {showDeleteModal && (
                                <DeleteProjectModal project={selectedProject} onClose={handleCloseDeleteModal}
                                                    onDelete={handleDeleteProject}/>
                            )}
                        </>
                }
            </Content>
        </Container>
    );
};