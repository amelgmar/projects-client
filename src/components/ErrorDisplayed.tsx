import styled from "styled-components";
import {FC} from "react";
import React from "react";

export const Wrapper = styled.div``

export interface ErrorDisplayedProps {
    message: string;
}

export const ErrorDisplayed: FC<ErrorDisplayedProps> = ({message}) => {
    return (
        <Wrapper>
            {message}
        </Wrapper>
    );
};