import React from "react";
import styled from "styled-components";
import {Table} from "../components/Table";
const Container = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`;

const Home = () => {
     return (
         <Container>
              <Table/>
         </Container>
     )

};

export default Home;
