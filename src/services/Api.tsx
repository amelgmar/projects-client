import axios from 'axios'

const DOMAIN_API_LINK = process.env.APP_DOMAIN_API_URL

const axiosInstance = axios.create({
    baseURL: DOMAIN_API_LINK,
    responseType: 'json',
    headers: {
        'Content-Type': 'application/json',
        Accept: '*/*'
    },
})
export default axiosInstance