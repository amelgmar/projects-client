// services/putAuthorAPI.ts

import {useEffect, useState} from "react";

const API_URL = process.env.VITE_DOMAIN_API_URL;

interface ApiState<T> {
    loading: boolean;
    error?: Error;
    results?: {rows: [], pageCount: number, totalCount: number} | any;
}

export function useFetchProjects<T>(page: number, pageSize: number, search?: string, sortField?: string, sortOrder?: string): ApiState<T> {
    const [state, setState] = useState<ApiState<T>>({ loading: true });

    useEffect(() => {
        async function fetchData() {
            try {
                const url = `${API_URL}/projects/?page=${page}&page_size=${pageSize}&search=${search}&ordering=${sortOrder === 'desc' ? '-' : ''}${sortField || 'id'}`;

                const response = await fetch(url);
                if (!response.ok) {
                    throw new Error(`HTTP error ${response.status}`);
                }
                const data = await response.json();
                setState({ loading: false, results:  {
                        rows: data.results,
                        pageCount: Math.ceil(data.count / pageSize),
                        totalCount: data.count,
                    }});
            } catch (error) {
                // @ts-ignore
                setState({ loading: false, error});
            }
        }
        fetchData();
    }, [page, pageSize, search, sortField, sortOrder]);

    return state;
}

export function usePostProject<T>(dataPayload: any, onSuccess?: (data: T) => void): ApiState<T> {
    const [state, setState] = useState<ApiState<T>>({ loading: true });

    useEffect(() => {
        async function postData() {
            try {
                const url = `${API_URL}/projects/`;

                const response = await fetch(url, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(dataPayload),
                });
                if (!response.ok) {
                    throw new Error(`HTTP error ${response.status}`);
                }
                const data = await response.json();
                setState({ loading: false, results:  data});
                if (onSuccess) {
                    onSuccess(data);
                }
            } catch (error) {
                // @ts-ignore
                setState({ loading: false, error });
            }
        }
        postData();
    }, [dataPayload]);

    return state;
}

export function useUpdateProject<T>(id?: number, dataPayload?: any, onSuccess?: (data: T) => void): ApiState<T> {
    const [state, setState] = useState<ApiState<T>>({ loading: true });

    useEffect(() => {
        async function updateData() {
            if (id && dataPayload) {
                try {
                    const url = `${API_URL}/projects/${id}/`;

                    const response = await fetch(url, {
                        method: 'PATCH',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify(dataPayload),
                    });
                    if (!response.ok) {
                        throw new Error(`HTTP error ${response.status}`);
                    }
                    const data = await response.json();
                    if (onSuccess) {
                        onSuccess(data);
                    }
                    setState({ loading: false, results:  data});
                } catch (error) {
                    // @ts-ignore
                    setState({ loading: false, error });
                }
            }
        }
        updateData();
    }, [dataPayload, id]);

    return state;
}

export function useDeleteProject<T>(id?: number, onSuccess?: (data: T) => void): ApiState<T> {
    const [state, setState] = useState<ApiState<T>>({ loading: true });

    useEffect(() => {
        async function deleteData() {
            if(id) {
                try {
                    const url = `${API_URL}/projects/${id}/`;

                    const response = await fetch(url, {
                        method: 'DELETE',
                    });
                    if (!response.ok) {
                        throw new Error(`HTTP error ${response.status}`);
                    }
                    const data = await response.json();
                    setState({ loading: false, results:  data});
                    if (onSuccess) {
                        onSuccess(data);
                    }
                } catch (error) {
                    // @ts-ignore
                    setState({ loading: false, error });
                }
            }
        }
        deleteData();
    }, [id]);

    return state;
}