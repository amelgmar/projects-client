import {defineConfig, createLogger, loadEnv} from 'vite'
import react from '@vitejs/plugin-react'

export default ({ mode }) => {
  // Load app-level env vars to node-level env vars.
  process.env = {...process.env, ...loadEnv(mode, process.cwd())};

  return defineConfig({
    define: {
      'process.env': process.env
    },
    server: {
      host: '0.0.0.0',
      port: 3000,
      hmr: {
        clientPort: parseInt(process.env.WEBSOCKET_PORT!),
      },
    },
    plugins: [react()],
    customLogger: createLogger('info', { prefix: '[coderpad]' }),
  })
}